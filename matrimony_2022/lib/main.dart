import 'package:flutter/material.dart';
import 'package:matrimony_2022/pre_login_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Matrimony',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: PreLoginPage(),
    );
  }
}
