import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_2022/utils/colors.dart';

class PreLoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Image.asset(
                  'assets/images/bg_matrimony.jpg',
                  fit: BoxFit.fitWidth,
                  width: double.infinity,
                ),
                Container(
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Image.asset(
                        'assets/images/bharatmatrimony-logo.png',
                        height: 90,
                        width: 170,
                      ),
                      Text(
                        'INDIA\'S\nMOST TRUSTED\nMATRIMONY BRAND',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 22,
                          fontFamily: 'RobotoMedium',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  color: loginBtnBgColor,
                  padding: const EdgeInsets.all(20),
                  child: Text(
                    'LOGIN',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 12,
                      fontFamily: 'RobotoMedium',
                    ),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  color: signupBtnBgColor,
                  padding: const EdgeInsets.all(20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'SIGNUP',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 12,
                          fontFamily: 'RobotoMedium',
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.fromLTRB(3, 0, 0, 0),
                        child: Icon(
                          Icons.arrow_forward_rounded,
                          color: Colors.white,
                          size: 10,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
